# RASPBERRY PI

TODO: need to find out board codes

 ## Boards information
https://www.raspberrypi.org/documentation/hardware/raspberrypi/revision-codes/README.md


## Power information
https://www.raspberrypi.org/documentation/hardware/raspberrypi/power/README.md

## Raspberry-pi 1 B+
    processor	: 0
    model name	: ARMv6-compatible processor rev 7 (v6l)
    BogoMIPS	: 697.95
    Features	: half thumb fastmult vfp edsp java tls 
    CPU implementer	: 0x41
    CPU architecture: 7
    CPU variant	: 0x0
    CPU part	: 0xb76
    CPU revision	: 7

    Hardware	: BCM2835
    Revision	: 1000010
    Serial		: 000000001a5b4323
    Model		: Raspberry Pi Model B Plus Rev 1.2

# Hostname

## Raspberry-pi 1 B
    rpi-1b

## Raspberry-pi 1 B+
    rpi-1bp

## Raspberry pi 4 B
    rpi-4b    