#!/bin/bash

# ---------------------------------------------------------
step_count=0 

print_message(){
    local message=$1
    local total_steps=$2
    step_count=$((step_count+1))
    echo -e "\e[31m ${step_count}/${total_steps} - $message \e[0m"
}

export -f print_message

# ---------------------------------------------------------
format_and_mount(){
    local partition=$1
    local partition_type=$2
    local folder=$3
    local total_steps=$4

    print_message "Create $partition_type filesystem" $total_steps
    $partition_type $partition

    print_message "Make $folder mount point" $total_steps
    mkdir $folder

    print_message "Mount the $partition" $total_steps
    mount $partition $folder
}

export -f format_and_mount
# ---------------------------------------------------------
create_partitions(){
    local disk=$1
    local total_steps=$2

    print_message "Create partitions" $total_steps

    (
    echo o # This will clear out any partitions on the drive
    echo n # Add a new partition
    echo p # Primary partition
    echo 1 # Partition number
    echo   # First sector (Accept default: 1)
    echo +200M # last sector 200M
    echo t # change partition type
    echo c # set the first partition to type W95 FAT32 (LBA)
    echo n # Add a new partition
    echo p # Primary partition
    echo 2 # Partition number
    echo   # First sector (Accept default)
    echo   # Last sector (Accept default)
    echo w # Write changes
    ) | fdisk $disk
}
export -f create_partitions
# ---------------------------------------------------------

# ---------------------------------------------------------
