# images handeling

## Create backup
dd if=/dev/sdX of=path/to/your-backup.img status=progress

### Compresed to save space
dd if=/dev/sdX status=progress | gzip -c > path/to/your-backup.img.gz 

## Restore the backup 
dd if=path/to/your-backup.img of=/dev/sdX status=progress

### Compresed image
gunzip -c /path/to/your-backup.img.gz | dd of=/dev/sdX status=progress
