#!/bin/bash

set -e
source $PWD/../functions/utils.sh

year="2021"
month="02"
day="05"
date="${year}-${month}-${day}" # date of the source files
source="/dev/mmcblk0"
destionation="./"
numSteps=2

do_backup() {
    local os=$1
    local board=$2
    
    echo "Selected OS $os"
    echo "Selected board $board"

    print_message "Creating backup of sd card" $numSteps
    dd if=${source} status=progress | gzip -c >${destionation}/${board}-${os}-${date}.img.gz

    print_message "THE END" $numSteps
}

select_board() {
    local os=$1
    select board in 'rpi-1b' 'rpi-1bp' 'rpi-4b' 'pcduino-1' 'pcduino-3'; do
        case $board in
        'rpi-1b' | 'rpi-1bp' | 'rpi-4b' | 'pcduino-1' | 'pcduino-3')
            echo "Selected board: $board"
            do_backup $os $board
            ;;
        *)
            echo "unknown board"
            echo "Selected number: $REPLY"
            ;;
        esac
        break
    done
}

select_os() {
    select os in 'arch' 'opensuse' 'raspios' 'ubuntu' 'opensuse'; do
        case $os in
        'arch' | 'opensuse' | 'raspios' | 'ubuntu' | 'opensuse')
            echo "Selected OS: $os"
            select_board $os
            ;;
        *)
            echo "unknown OS"
            echo "Selected number: $REPLY"
            ;;
        esac
        break
    done
}

select_os
