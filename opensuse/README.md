## OpenSuse

https://en.opensuse.org/Portal:ARM


## Default Credentials
u: root
p: linux

## Install
replace sdX with the device name of your SD card
```
 xzcat [image].raw.xz | dd bs=4M of=/dev/sdX iflag=fullblock oflag=direct status=progress; sync
```


### Change hostname
    sudo hostnamectl set-hostname amb-pcname

### Disable grapgical boot
systemctl set-default multi-user

### Update the system
    zypper ref
    zypper update
    zypper dup 

### Disable swap 
    vi /etc/fstab
    swapoff -a 
    free -h

#### remove partition it using yast ?

### Connect to WIFI
    nmcli radio
    nmcli device wifi rescan
    nmcli device wifi list
    nmcli device wifi connect SSID-Name password wireless-password

### Install docker
    zypper install docker docker-compose
    sudo systemctl enable docker
    sudo usermod -G docker -a $USER
    sudo systemctl restart docker