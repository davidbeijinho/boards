echo -e "\e[31m-1/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mUPDATE \e[0m"
sudo apt-get update -y
echo -e "\e[31m-2/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mUPGRADE \e[0m"
sudo apt-get upgrade -y
echo -e "\e[31m-3/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mFULL UPGRADE \e[0m"
sudo apt full-upgrade -y
echo -e "\e[31m-4/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mRUN AUTOREMOVE \e[0m"
sudo apt autoremove
echo -e "\e[31m-5/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mDOWNLOAD NODE \e[0m"
curl -o node.tar.gz https://nodejs.org/dist/v10.16.3/node-v10.16.3-linux-armv6l.tar.gz
echo -e "\e[31m-6/12-------------------------------------------------------------------------------------------------"
echo -e "CREATE NODE DIRECTORY \e[0m"
mkdir ~/node
echo -e "\e[31m-7/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mUNTAR NODE \e[0m"
tar -xzf node.tar.gz --directory ~/node
echo -e "\e[31m-8/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mCOPY NODE \e[0m"
sudo cp -R ~/node/node*/** /usr/local/
echo -e "\e[31m-9/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mREMOVE NODE DIR \e[0m"
rm -fr ~/node
echo -e "\e[31m-10/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mREMOVE NODE TAR \e[0m"
rm ~/node.tar.gz
#echo -e "\e[31m-11/12-------------------------------------------------------------------------------------------------"
#echo -e "\e[31mCONFIG NPM \e[0m"
#mkdir ~/.npm-global
#npm config set prefix '~/.npm-global'
#export PATH=~/.npm-global/bin:$PATH
#source ~/.profile
echo -e "\e[31m-12/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mINSTALL PM2 \e[0m"
sudo npm install -g pm2
echo -e "\e[31m-12/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mINSTALL PM2 monit \e[0m"
pm2 install pm2-server-monit
echo -e "\e[31m-12/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mINSTALL GIT \e[0m"
sudo apt-get install git -y
echo -e "\e[31m-12/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mINSTALL NGINX \e[0m"
sudo apt-get install nginx -y
echo -e "\e[31m-12/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mSTART NGINX \e[0m"
sudo /etc/init.d/nginx start
echo -e "\e[31m-12/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mAUTO BOOT NGINX \e[0m"
sudo update-rc.d nginx defaults
echo -e "\e[31m-12/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mINSTALL CERTBOT NGINX \e[0m"
sudo apt-get install python-certbot-nginx -y
echo -e "\e[31m-12/12-------------------------------------------------------------------------------------------------"
echo -e "\e[31mINSTALL NO-IP  \e[0m"
mkdir /home/pi/noip
wget https://www.noip.com/client/linux/noip-duc-linux.tar.gz -P /home/pi/noip
tar /home/pi/noip/vzxf noip-duc-linux.tar.gz
sudo make -C /home/pi/noip/noip-2.1.9-1
echo -e "\e[5m\e[31mRUN sudo make install to finish no-ip install"
echo -e "\e[31m--------------------------------------------------------------------------------------------------"
echo -e "\e[5m\e[31mFINISH"
echo -e "\e[31m-------------------------------------------------------------------------------------------------- \e[0m"


echo "https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md"
echo "https://help.github.com/en/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent"
echo "https://help.github.com/en/articles/adding-a-new-ssh-key-to-your-github-account"
echo "https://www.noip.com/support/knowledgebase/install-ip-duc-onto-raspberry-pi/"
