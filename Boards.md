# BOARDS

## Raspberry pi

Rasp1 B

    Raspberry pi model B

Rasp1 B+
    
    Raspberry pi model B+ V1.2

Rasp4 B

    Raspberry pi 4 model B

---

## PcDuino

PcDuino3Nano Lite
    
    LinkSprite pcDuino3Nano Lite

https://www.linksprite.com/linksprite-pcduino3nano-lite/

PcDuino Lite
    
    LinkSprite pcDuino Lite

https://www.linksprite.com/linksprite-pcduino-lite/

---
