#!/bin/bash

# Initialize the pacman keyring 
pacman-key --init
# populate the Arch Linux ARM package signing keys: 
pacman-key --populate archlinuxarm

# Do a full upgrade
pacman -Syyu -y

# Set Hostname
echo "rpi-1bp" > /etc/hostname
