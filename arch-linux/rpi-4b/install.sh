#!/bin/bash
set -e
disk="/dev/mmcblk0"
firstPartition="p1" # boot
secondPartition="p2" # root
fileSystemURL="http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-4-latest.tar.gz"
bootFolder="./boot"
rootFolder="./root"
numSteps=18

echo -e "\e[31m 1/${numSteps} - Zero the beginning of the SD card \e[0m"
dd if=/dev/zero of=$disk bs=1M count=8

echo -e "\e[31m 2/${numSteps} - Create partition \e[0m"
(
echo o # This will clear out any partitions on the drive
echo n # Add a new partition
echo p # Primary partition
echo 1 # Partition number
echo   # First sector (Accept default: 1)
echo +200M # last sector 200M
echo t # change partition type
echo c # set the first partition to type W95 FAT32 (LBA)
echo n # Add a new partition
echo p # Primary partition
echo 2 # Partition number
echo   # First sector (Accept default)
echo   # Last sector (Accept default)
echo w # Write changes
) | fdisk $disk


echo -e "\e[31m 3/${numSteps} - Create FAT filesystem \e[0m"
mkfs.vfat "${disk}${firstPartition}"

echo -e "\e[31m 4/${numSteps} - Make boot mount point \e[0m"
mkdir $bootFolder

echo -e "\e[31m 5/${numSteps} - Mount the FAT filesystem \e[0m"
mount "${disk}${firstPartition}" $bootFolder

echo -e "\e[31m 6/${numSteps} - Create the ext4 filesystem \e[0m"
mkfs.ext4 "${disk}${secondPartition}"

echo -e "\e[31m 7/${numSteps} - Make root mount point \e[0m"
mkdir $rootFolder

echo -e "\e[31m 8/${numSteps} - mount the ext4 filesystem \e[0m" 
mount "${disk}${secondPartition}" $rootFolder

echo -e "\e[31m 9/${numSteps} - Download root filesystem \e[0m"
wget $fileSystemURL -O archLinux.tar.gz

echo -e "\e[31m 10/${numSteps} - Extract root filesystem \e[0m"
tar -zxvf archLinux.tar.gz -C $rootFolder

echo -e "\e[31m 11/${numSteps} - Sync \e[0m"
sync

echo -e "\e[31m 12/${numSteps} - Move boot files to the first partition \e[0m"
mv ${rootFolder}/boot/* $bootFolder

echo -e "\e[31m 13/${numSteps} - Unmount root folder \e[0m"
umount $rootFolder

echo -e "\e[31m 14/${numSteps} - Unmount boot folder \e[0m"
umount $bootFolder

echo -e "\e[31m 15/${numSteps} - Remove root folder \e[0m"
rm -r $rootFolder

echo -e "\e[31m 16/${numSteps} - Remove boot folder \e[0m"
rm -r $bootFolder

echo -e "\e[31m 17/${numSteps} - Remove root filesystem \e[0m"
rm archLinux.tar.gz

echo -e "\e[31m ${numSteps}/${numSteps} - THE END \e[0m"
