# Arch Linux

## Credentials user
```
    user: alarm
    password: alarm
```
## Credentials Admin
```
    user: root
    password: root
```

## Init pacman
    pacman-key --init
    pacman-key --populate archlinuxarm

## Install sudo
```
    pacman -S sudo
    # edit /etc/sudoers with visudo
    # Uncomment to allow members of group wheel to execute any command
    %wheel ALL=(ALL) ALL
```

## Change Hostname
https://wiki.archlinux.org/index.php/Installation_guide#Network_configuration


### Change root password
https://wiki.archlinux.org/index.php/Installation_guide#Root_password


## Connect to Wifi
https://wiki.archlinux.org/index.php/Wpa_supplicant

```
enable wpa_supplant
enable dhcpd
```

## Static ip
```
#/etc/systemd/network/eth0.network
[Match]
Name=eth0

[Network]
Address=192.168.1.102/24
Gateway=192.168.1.1
DNS=8.8.8.8
DNS=8.8.4.4
```

## restart networkd service
```
systemctl restart  systemd-networkd.service
 ```
 
systemctl --type=service --no-pager
## keyring problems
```
pacman-key --init
pacman-key --populate
#pacman-key --refresh-keys
pacman -Sy archlinux-keyring
```

## DNS
```
sudo netstat -putan | grep 53
sudo systemctl disable systemd-resolved
sudo systemctl stop systemd-resolved

```
### Others
https://wiki.archlinux.org/index.php/Installation_guide

https://wiki.archlinux.org/index.php/General_recommendations

https://wiki.archlinux.org/index.php/Category:System_administration
