#!/bin/bash
set -e
disk="/dev/mmcblk0"
# device="pcduino3-nano"
device="pcduino"
firstPartition="p1"
fileSystemURL="http://os.archlinuxarm.org/os/ArchLinuxARM-armv7-latest.tar.gz"
mountFolder="./mnt"
ubootURL="http://os.archlinuxarm.org/os/sunxi/boot/${device}/u-boot-sunxi-with-spl.bin"
bootURL="http://os.archlinuxarm.org/os/sunxi/boot/${device}/boot.scr"
numSteps=17

echo -e "\e[31m 1/${numSteps} - Zero the beginning of the SD card \e[0m"
dd if=/dev/zero of=$disk bs=1M count=8

echo -e "\e[31m 2/${numSteps} - Create partition \e[0m"
(
echo o # Create a new empty DOS partition table
echo n # Add a new partition
echo p # Primary partition
echo 1 # Partition number
echo   # First sector (Accept default: 1)
echo   # Last sector (Accept default: varies)
echo w # Write changes
) | fdisk $disk

echo -e "\e[31m 3/${numSteps} - Create ext4 partition \e[0m"
mkfs.ext4 "${disk}${firstPartition}"

echo -e "\e[31m 4/${numSteps} - Make mount point \e[0m"
mkdir $mountFolder

echo -e "\e[31m 5/${numSteps} - Mount filesystem \e[0m"
mount "${disk}${firstPartition}" $mountFolder 

echo -e "\e[31m 6/${numSteps} - Download root filesystem \e[0m"
wget $fileSystemURL -O archLinux.tar.gz

echo -e "\e[31m 7/${numSteps} - Extract root filesystem \e[0m"
tar -zxvf archLinux.tar.gz -C $mountFolder

echo -e "\e[31m 8/${numSteps} - Sync \e[0m"
sync

echo -e "\e[31m 9/${numSteps} - Clean root filesystem \e[0m"
rm archLinux.tar.gz

echo -e "\e[31m 10/${numSteps} - Download u-boot \e[0m"
wget $ubootURL -O uboot.bin

echo -e "\e[31m 11/${numSteps} - Write u-boot \e[0m"
dd if=uboot.bin of=$disk bs=1024 seek=8

echo -e "\e[31m 12/${numSteps} - Clean u-boot \e[0m"
rm uboot.bin 

echo -e "\e[31m 13/${numSteps} - Download boot \e[0m"
wget $bootURL -O "${mountFolder}/boot/boot.scr"

echo -e "\e[31m 14/${numSteps} - Unmount SD \e[0m"
umount $mountFolder

echo -e "\e[31m 15/${numSteps} - Final sync \e[0m"
sync

echo -e "\e[31m 16/${numSteps} - Remove mount point \e[0m"
rm -r $mountFolder

echo -e "\e[31m ${numSteps}/${numSteps} - THE END \e[0m"
