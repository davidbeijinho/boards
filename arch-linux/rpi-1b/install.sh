#!/bin/bash
set -e
disk="/dev/mmcblk0"
firstPartition="p1" # boot
secondPartition="p2" # root
fileSystemURL="http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-latest.tar.gz"
bootFolder="./boot"
rootFolder="./root"
numSteps=18

source $PWD/../../utils/functions/utils.sh

print_message "Zero the beginning of the SD card" $numSteps
dd if=/dev/zero of=$disk bs=1M count=8

create_partitions $disk $numSteps

boot_partion=${disk}${firstPartition}
format_and_mount $boot_partion "mkfs.vfat" $bootFolder $numSteps

root_partion=${disk}${secondPartition}
format_and_mount $root_partion "mkfs.ext4" $rootFolder $numSteps

print_message "Download root filesystem" $numSteps
wget $fileSystemURL -O archLinux.tar.gz

print_message "Extract root filesystem" $numSteps
tar -zxvf archLinux.tar.gz -C $rootFolder

print_message "Sync" $numSteps
sync

print_message "Move boot files to the first partition" $numSteps
mv ${rootFolder}/boot/* $bootFolder

print_message "Unmount root folder" $numSteps
umount $rootFolder

print_message "Unmount boot folder" $numSteps
umount $bootFolder

print_message "Remove root folder" $numSteps
rm -r $rootFolder

print_message "Remove boot folder" $numSteps
rm -r $bootFolder

print_message "Remove root filesystem" $numSteps
rm archLinux.tar.gz

print_message "THE END" $numSteps
